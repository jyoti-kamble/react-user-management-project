import styles from "./UserDetails.module.css";

const UserDetails = ({ user }) => {
  return (
    <>
      {user ? (
        <section className={styles["user-details"]}>
          <h2>{user.name}</h2>
          <span className={styles.username}>{user.username}</span> |
          <span>{user.email}</span>
          {/* ASSIGNMENT ---> POPULATE THIS DYNAMICALLY */}
          <p>
            <h5>Address:</h5> 
            <p>Street: {user.address.street} </p>
            <p>Suite: {user.address.suite}</p>
            <p> City: {user.address.city}</p>
            <p>Zipcode: {user.address.zipcode}</p>
          </p>
          <p>Phone: {user.phone}</p>
          <p>
            <h5>Work Details:</h5>
            <p>Company name: {user.company.name}</p>
            <p>Catch Phrase: {user.company.catchPhrase}</p>
            <p>BS: {user.company.bs}</p>
          </p>
          <p>Website: {user.website}</p>
        </section>
      ) : (
        <section className={styles["user-details"]}>
          <p>No User Selected</p>
        </section>
      )}
    </>
  );
};

export default UserDetails;
